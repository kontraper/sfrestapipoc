public class TagsManager {
    
    public static Set<String> currentTagsSet(String tags){
        List<String> tagsList;
        Set<String> tagsSet = new Set<String>();
        
        if(String.isNotBlank(tags)){
            tagsList = tags.split(';');
            for(String tagsIterator: tagsList){
                tagsSet.add(tagsIterator);
            }    
        }
        return tagsSet;
    }
    
    public static String newTagsFromSet(String updatedTags, Set<String> tagsSet){
        Integer i=0;
        for(String tagSetIterator: tagsSet){
            if(updatedTags==null){
                updatedTags=tagSetIterator+';';
                i++;
            }else{
                if(i==tagsSet.size()-1){
                    updatedTags+=tagSetIterator;
                }else{
                    updatedTags+=tagSetIterator+';';
                    i++; 
                }    
            }
        }
        return updatedTags;
    }
    
    public static void removeTags(String tagsToRemove, Set<String> tagsSet){
        List<String> tagsToRemoveList = tagsToRemove.split(';');
        for(String tagsToRemoveListIterator: tagsToRemoveList){
            tagsSet.remove(tagsToRemoveListIterator);
        }
    }
    
    public static void addTags(String tagsToAdd, Set<String> tagsSet){
        List<String> tagsToAddList = tagsToAdd.split(';');
        for(String tagsToAddListIterator: tagsToAddList){
            tagsSet.add(tagsToAddListIterator);
        }  
    }
    
}