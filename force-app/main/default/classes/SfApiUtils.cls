public class SfApiUtils {
     public static void setResponse(Integer statusCode,
                                    SfRestStatus status,
                                    String message,
                                    List<Object> objList){
                                        RestResponse restRes = RestContext.response;
                                        SfRestResponse response = new SfRestResponse(status,message);
                                        if(objList!=null)response.setData(objList);
                                        restRes.statusCode = statusCode;
                                        restRes.responseBody = Blob.valueOf(JSON.serialize(response, true));        
                                    }
}