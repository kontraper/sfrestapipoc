@RestResource(urlMapping ='/notesTags/*')
global class SfRestNotesTagsController {
    
    @HttpPut
    global static void updateNotesTags(){
        Id id = null;
        String tagsToAdd = null;
        String tagsToRemove = null;
        
        RestResponse restRes = RestContext.response;
        RestRequest restReq = RestContext.request;
        id = restReq.params.get('id');
        tagsToAdd = restReq.params.get('tags_to_add');
        tagsToRemove = restReq.params.get('tags_to_remove');
        
        
        if(String.isBlank(id)){
            SfApiUtils.setResponse(400,SfRestStatus.ERROR,'No Id for note set', null);   
        }else{
            try{
                
                Note__c note = [SELECT Tags__c FROM Note__c WHERE Id=:id];
                List<Note__c> noteList = new List<Note__c>();
                
                Set<String> tagsSet = TagsManager.currentTagsSet(note.Tags__c);
                String updatedTags;
                
                if(String.isNotBlank(tagsToAdd)){
                    
                    TagsManager.addTags(tagsToAdd,tagsSet);
                    
                    updatedTags = TagsManager.newTagsFromSet(updatedTags,tagsSet);
                    
                    note.Tags__c = updatedTags;                
                    update note;
                    
                    noteList.add(note);
                    SfApiUtils.setResponse(200,SfRestStatus.OK,'Updated', noteList);     
                    
                }else if(String.isNotBlank(tagsToRemove)){
                    
                    TagsManager.removeTags(tagsToRemove,tagsSet);
                    
                    updatedTags = TagsManager.newTagsFromSet(updatedTags,tagsSet);
                    
                    note.Tags__c = updatedTags;                
                    update note;
                    
                    noteList.add(note);
                    SfApiUtils.setResponse(200,SfRestStatus.OK,'Updated', noteList);  
                    
                }else{
                    noteList.add(note); 
                    SfApiUtils.setResponse(403,SfRestStatus.WARNING,'Not modified', noteList);     
                }
                
            }catch(QueryException e){
                SfApiUtils.setResponse(404,SfRestStatus.WARNING,'No note found with given id', null);         
            }
        }        
    } 
    
    @HttpGet
    global static void getNotesByTags(){
        String tags = null;
        String explicitSearch= null;
        
        RestResponse restRes = RestContext.response;
        RestRequest restReq = RestContext.request;
        tags = restReq.params.get('tags');
        explicitSearch  = restReq.params.get('explicit_search');
        
        if(String.isBlank(explicitSearch)) explicitSearch ='false'; 
        
        if(!explicitSearch.equals('true')&&!explicitSearch.equals('false')){
            
            SfApiUtils.setResponse(400,SfRestStatus.ERROR,'explicit_search must be boolean value', null);   
            
        }else if(String.isBlank(tags)){
            
            SfApiUtils.setResponse(400,SfRestStatus.ERROR,'No tags for note set', null);   
            
        }else{
            try{
                Set<String> reqTagsSet = new Set<String>();
                TagsManager.addTags(tags,reqTagsSet);
                List<Note__c> noteList =[SELECT Id, Content__c, Tags__c, Title__c FROM Note__c WHERE Tags__c like:'%'+tags+'%'];
                Set<Note__c> filteredNotes = new Set<Note__c>();
                
                if(explicitSearch.equals('false')){
                    
                    for(Note__c noteIterator: noteList){
                        Set<String> currentTagsSet = TagsManager.currentTagsSet(noteIterator.Tags__c);
                        for(String reqTag: reqTagsSet){           
                            if(currentTagsSet.contains(reqTag)){
                                filteredNotes.add(noteIterator);
                            }
                        }                    
                    }
                    List<Note__c> newFilteredNotesList=new List<Note__c>(filteredNotes);
                    
                    if(filteredNotes.isEmpty()){
                        SfApiUtils.setResponse(404,SfRestStatus.WARNING,'No note found with given tags, , explicit search: '+explicitSearch, null);     
                    }else{
                        SfApiUtils.setResponse(200,SfRestStatus.OK,'OK', newFilteredNotesList);     
                    }
                }
                
                if(explicitSearch.equals('true')){
                    
                    for(Note__c noteIterator: noteList){
                        Set<String> tagsSet = TagsManager.currentTagsSet(noteIterator.Tags__c);
                        Integer i=0;
                        for(String reqTag: reqTagsSet){  
                            if(tagsSet.contains(reqTag)){
                                i++;   
                            }   
                        } 
                        if(i==reqTagsSet.size()){
                           filteredNotes.add(noteIterator);                           
                        }      
                    }
                    List<Note__c> newFilteredNotesList=new List<Note__c>(filteredNotes);
                    
                    if(filteredNotes.isEmpty()){
                        SfApiUtils.setResponse(404,SfRestStatus.WARNING,'No note found with given tags, explicit search: '+explicitSearch, null);     
                    }else{
                        SfApiUtils.setResponse(200,SfRestStatus.OK,'OK', newFilteredNotesList);     
                    }      
                }
                
            }catch(QueryException e){
                SfApiUtils.setResponse(404,SfRestStatus.WARNING,'No note found with given tags', null);         
            }
        }        
    } 
    
}