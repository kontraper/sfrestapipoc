public class SfRestResponse {

    private String status;
    private String message;
    private List<Object> data;
    
    public SfRestResponse(SfRestStatus status, String message){
        this.status = status.name();
        this.message = message;
    }  
      
    public void setData(List<Object> data){
        this.data=data;
    }
    public List<Object> getData(){
        return data;
    }
    
}