@RestResource(urlMapping ='/notes/*')
global class SfRestNotesController {
    
    @HttpGet
    global static void getNotes(){
        String id = null;
        
        RestResponse restRes = RestContext.response;
        RestRequest restReq = RestContext.request;
        id = restReq.params.get('id');
        
        if(id!=null){
            if(String.isBlank(id)){
                SfApiUtils.setResponse(200,SfRestStatus.ERROR,'No Id for note set', null);
            }else{
                try{
                    Note__c note = [SELECT Content__c, Title__c FROM Note__c WHERE Id=:id];
                    List<Note__c> noteList = new List<Note__c>();
                    noteList.add(note);
                    SfApiUtils.setResponse(200,SfRestStatus.OK,'OK', noteList);
                }catch(QueryException e){                    
                    SfApiUtils.setResponse(404,SfRestStatus.WARNING,'No note found with given id', null);
                }
            }
            
        }else{
            List<Note__c> noteList = [SELECT Content__c, Title__c FROM Note__c];
            SfApiUtils.setResponse(200,SfRestStatus.OK,'OK', noteList);       
        }   
    } 
    
    @HttpPost
    global static void createNotes(){
        String content = null;
        String title = null;
        Id clientId = null;
        
        RestResponse restRes = RestContext.response;
        RestRequest restReq = RestContext.request;
        
        clientId = restReq.params.get('clientId');
        content = restReq.params.get('content');
        title = restReq.params.get('title');
        
        if(content==null){
            SfApiUtils.setResponse(400,SfRestStatus.ERROR,'No content for Note set', null);   
        }else if(title==null){
            SfApiUtils.setResponse(400,SfRestStatus.ERROR,'No title for Note set', null);   
        }else if(clientId==null){
            SfApiUtils.setResponse(400,SfRestStatus.ERROR,'No client Id for Note set', null);   
        }else{
            
            Note__c note = new Note__c();
            note.Content__c = content;
            note.Title__c = title;
            note.Lead__c = clientId;
            insert note;
            
            List<Note__c> noteList = new List<Note__c>();
            noteList.add(note);
            
            SfApiUtils.setResponse(201,SfRestStatus.OK,'Created', noteList);   
        }   
    }  
    
    @HttpPut
    global static void updateNotes(){
        Id id = null;
        String content = null;
        String title = null;
        
        RestResponse restRes = RestContext.response;
        RestRequest restReq = RestContext.request;
        id = restReq.params.get('id');
        content = restReq.params.get('content');
        title = restReq.params.get('title');
        
        if(String.isBlank(id)){
            SfApiUtils.setResponse(400,SfRestStatus.ERROR,'No Id for note set', null);   
        }else{
            try{
                Note__c note = [SELECT Content__c, Title__c FROM Note__c WHERE Id=:id];
                List<Note__c> noteList = new List<Note__c>();
                
                if(String.isNotBlank(content)||String.isNotBlank(title)){
                    if(String.isNotBlank(content)) note.Content__c = content;                
                    if(String.isNotBlank(title)) note.Title__c = title;
                    update note;
                    
                    noteList.add(note);
                    SfApiUtils.setResponse(200,SfRestStatus.OK,'Updated', noteList);     
                }else{
                    noteList.add(note); 
                    SfApiUtils.setResponse(403,SfRestStatus.WARNING,'Not modified', noteList);     
                }
                
            }catch(QueryException e){
                SfApiUtils.setResponse(404,SfRestStatus.WARNING,'No note found with given id', null);         
            }
        }        
    } 
    
    @HttpDelete
    global static void deleteNotes(){
        Id id = null;
        
        RestResponse restRes = RestContext.response;
        RestRequest restReq = RestContext.request;
        id = restReq.params.get('id');
        
        if(String.isBlank(id)){
            SfApiUtils.setResponse(400,SfRestStatus.ERROR,'No Id for note set', null);   
        }else{
            try{
                Note__c note = [SELECT Content__c, Title__c FROM Note__c WHERE Id=:id];
                delete note;
                
                List<Note__c> noteList = new List<Note__c>();
                noteList.add(note);   
                SfApiUtils.setResponse(200,SfRestStatus.OK,'Deleted', null);
                
            }catch(QueryException e){
                SfApiUtils.setResponse(404,SfRestStatus.WARNING,'No note found with given id', null);
            }
        }        
    } 
}