# Salesforce Rest API

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

### API
| type | method | params | type of param |
| ------ | ------ | ------ |  ------ |
| *GET* | /api/notes | [id] | string |
| *GET* | /api/notesTags | tags, explicit_search | string, boolean |
| *POST* | /api/notes | title, content, clientId | string, string, string |
| *PUT* | /api/notes | id, title, content | string, string, string |
| *PUT* | /api/notesTags | id, tags_to_add, tags_to_remove | string, string, string |
| *DELETE* | /api/notes | id | string |


### Examples of usage

# GET /notes
#### Descripition:
Returns all notes or one by id

```sh
$ curl -X GET https://mysitefordeveloping123-developer-edition.eu25.force.com/services/apexrest/notes
```

```json
{  
   "status":"OK",
   "message":"OK",
   "data":[  
      {  
         "attributes":{  
            "type":"Note__c",
            "url":"/services/data/v46.0/sobjects/Note__c/a012o00001F8rTKAAZ"
         },
         "Content__c":"test",
         "Title__c":"test",
         "Id":"a012o00001F8rTKAAZ"
      }
   ]
}
```

# GET /notesTags
#### Descripition:
    * Returns notes by multiple tags - each note should match all of the given tags (explicit_search=true)
    * Returns notes by multiple tags each note should match any of the given tags  (explicit_search=false)
    
    if there are multiple tags, they must be semicolon separated

```sh
$ curl -X GET https://mysitefordeveloping123-developer-edition.eu25.force.com/services/apexrest/notesTags?tags=test;abcdtag&explicit_search=true
```
### explicit_search=true

```json
{  
   "status":"OK",
   "message":"OK",
   "data":[  
      {  
         "attributes":{  
            "type":"Note__c",
            "url":"/services/data/v46.0/sobjects/Note__c/a012o00001F8ojEAAR"
         },
         "Id":"a012o00001F8ojEAAR",
         "Content__c":"test",
         "Tags__c":"test;abcdtag",
         "Title__c":"test"
      }
   ]
}

```
### explicit_search=false

```sh
$ curl -X GET https://mysitefordeveloping123-developer-edition.eu25.force.com/services/apexrest/notesTags?tags=testg&explicit_search=false
```

```json
{  
   "status":"OK",
   "message":"OK",
   "data":[  
      {  
         "attributes":{  
            "type":"Note__c",
            "url":"/services/data/v46.0/sobjects/Note__c/a012o00001F8ojEAAR"
         },
         "Id":"a012o00001F8ojEAAR",
         "Content__c":"test",
         "Tags__c":"test;abcdtag",
         "Title__c":"test"
      }
   ]
}

```
```json
{  
   "status":"ERROR",
   "message":"No tags for note set"
}
```

# POST /notes
#### Descripition:
Creates note with given parameters

```sh
curl -d "title=test title&content=test content&clientId=00Q2o000014sqd8" -X POST https://mysitefordeveloping123-developer-edition.eu25.force.com/services/apexrest/notes

```

```json
{  
   "status":"OK",
   "message":"Created",
   "data":[  
      {  
         "attributes":{  
            "type":"Note__c",
            "url":"/services/data/v46.0/sobjects/Note__c/a012o00001F8rTjAAJ"
         },
         "Content__c":"test",
         "Title__c":"test",
         "Lead__c":"00Q2o000014sqd8EAA",
         "Id":"a012o00001F8rTjAAJ"
      }
   ]
}
```
```json
{  
   "status":"ERROR",
   "message":"No client Id for Note set"
}
```


# PUT /notes
#### Descripition:
Edit note (by id) with given parameters

```sh
$ curl -d "title=test title&content=test content" -X PUT https://mysitefordeveloping123-developer-edition.eu25.force.com/services/apexrest/notes?id=a012o00001F8rTjAAJ
```

```json
{  
   "status":"OK",
   "message":"Updated",
   "data":[  
      {  
         "attributes":{  
            "type":"Note__c",
            "url":"/services/data/v46.0/sobjects/Note__c/a012o00001F8cg5AAB"
         },
         "Content__c":"test",
         "Title__c":"nowytytul",
         "Id":"a012o00001F8cg5AAB"
      }
   ]
}
```

```json
{  
   "status":"ERROR",
   "message":"No Id for note set"
}
```

# PUT /notesTags
#### Descripition:
Edit note tags (by id) with given parameters - tags_to_add, tags_to_remove
if there are multiple tags, they must be semicolon separated

```sh
$ curl -d "tags_to_add="test;abcd;eee" title&content=test content" -X PUT https://mysitefordeveloping123-developer-edition.eu25.force.com/services/apexrest/notes?id=a012o00001F8rTjAAJ
```

```sh
$ curl -d "tags_to_remove="test;abcd;eee" title&content=test content" -X PUT https://mysitefordeveloping123-developer-edition.eu25.force.com/services/apexrest/notes?id=a012o00001F8rTjAAJ
```

```json
{  
   "status":"OK",
   "message":"Updated",
   "data":[  
      {  
         "attributes":{  
            "type":"Note__c",
            "url":"/services/data/v46.0/sobjects/Note__c/a012o00001F8ojEAAR"
         },
         "Id":"a012o00001F8ojEAAR",
         "Tags__c":"test;abcdtag"
      }
   ]
}
```

```json
{  
   "status":"ERROR",
   "message":"No Id for note set"
}
```

# DELETE /notes
#### Descripition:
Deletes note (by id)

```sh
$ curl -X DELETE https://mysitefordeveloping123-developer-edition.eu25.force.com/services/apexrest/notes?id=a012o00001F8rTjAAJ
```

```json
{  
   "status":"OK",
   "message":"Deleted"
}
```

```json
{  
   "status":"ERROR",
   "message":"No Id for note set"
}
```


